package ru.tsc.korosteleva.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface ILoggerService {

    void info(@Nullable String massage);

    void debug(@Nullable String massage);

    void command(@Nullable String massage);

    void error(@Nullable Exception e);

}
