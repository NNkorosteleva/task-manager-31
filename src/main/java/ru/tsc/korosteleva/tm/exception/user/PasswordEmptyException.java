package ru.tsc.korosteleva.tm.exception.user;

public final class PasswordEmptyException extends AbstractUserException {

    public PasswordEmptyException() {
        super("Error! Password is empty.");
    }

}
