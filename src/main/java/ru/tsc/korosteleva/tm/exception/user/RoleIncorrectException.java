package ru.tsc.korosteleva.tm.exception.user;

import ru.tsc.korosteleva.tm.exception.field.AbstractFieldException;

public final class RoleIncorrectException extends AbstractFieldException {

    public RoleIncorrectException(String value) {
        super("Error! Role ``" + value + "`` is incorrect.");
    }

}
