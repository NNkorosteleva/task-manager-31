package ru.tsc.korosteleva.tm.exception.field;

import ru.tsc.korosteleva.tm.exception.AbstractException;

public abstract class AbstractFieldException extends AbstractException {

    public AbstractFieldException() {
    }

    public AbstractFieldException(String message) {
        super(message);
    }

}
